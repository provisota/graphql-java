package com.howtographql.hackernews.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.howtographql.hackernews.dto.SigninPayload;
import com.howtographql.hackernews.dto.User;

/**
 * @author Ilya Alterovych on 28-Mar-18
 */
public class SigninResolver implements GraphQLResolver<SigninPayload> {

    public User user(SigninPayload payload) {
        return payload.getUser();
    }
}