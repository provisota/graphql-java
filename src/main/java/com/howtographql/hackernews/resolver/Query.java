package com.howtographql.hackernews.resolver;

import com.coxautodev.graphql.tools.GraphQLRootResolver;
import com.howtographql.hackernews.dto.Link;
import com.howtographql.hackernews.dto.User;
import com.howtographql.hackernews.dto.Vote;
import com.howtographql.hackernews.repository.LinkRepository;
import com.howtographql.hackernews.repository.UserRepository;
import com.howtographql.hackernews.repository.VoteRepository;

import java.util.List;

/**
 * @author Ilya Alterovych on 27-Mar-18
 */
public class Query implements GraphQLRootResolver {

    private final LinkRepository linkRepository;
    private final UserRepository userRepository;
    private final VoteRepository voteRepository;

    public Query(LinkRepository linkRepository, UserRepository userRepository, VoteRepository voteRepository) {
        this.linkRepository = linkRepository;
        this.userRepository = userRepository;
        this.voteRepository = voteRepository;
    }

    // link
    public List<Link> allLinks() {
        return linkRepository.getAllLinks();
    }

    public Link findLinkById(String id) {
        return linkRepository.findById(id);
    }

    // user
    public List<User> allUsers() {
        return userRepository.getAllUsers();
    }

    public User findUserById(String id) {
        return userRepository.findById(id);
    }

    //vote
    public List<Vote> allVotes() {
        return voteRepository.getAllVotes();
    }
}