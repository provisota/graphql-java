package com.howtographql.hackernews.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

/**
 * @author Ilya Alterovych on 27-Mar-18
 */
@AllArgsConstructor
@Getter
public class Link {

    private final String id;
    @NotNull
    private final String url;
    private final String description;
    private final String userId;

    public Link(@NotNull String url, String description) {
        this(null, url, description, null);
    }

    public Link(@NotNull String url, String description, String userId) {
        this(null, url, description, userId);
    }
}