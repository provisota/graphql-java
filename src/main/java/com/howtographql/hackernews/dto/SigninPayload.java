package com.howtographql.hackernews.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Ilya Alterovych on 28-Mar-18
 */
@AllArgsConstructor
@Getter
public class SigninPayload {

    private final String token;
    private final User user;
}