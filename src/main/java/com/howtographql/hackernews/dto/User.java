package com.howtographql.hackernews.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Ilya Alterovych on 28-Mar-18
 */
@AllArgsConstructor
@Getter
public class User {

    private final String id;
    private final String name;
    private final String email;
    private final String password;

    public User(String name, String email, String password) {
        this(null, name, email, password);
    }
}